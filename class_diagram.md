# Rectangle class diagram
```plantuml
index : canvas
main o-- menu
main o-- stage
index o-- main

main : number canvasHeight
main : number canvasWidth
main : loop()

menu o-- menuModel
menu o-- menuController
menu : window.addeventListener('keyDown')
menu : window.addeventListener('keyUp')


menuController : keyListener()
menuController : mouseListener()


stage : window.addeventListener('keyDown')
stage : window.addeventListener('keyUp')

stage o-- stageModel
stage o-- stageController
stageController : keyListener()
stageController : mouseListener()

stageModel -- recModel
stageModel -- platformModel
```

# Rectangle class diagram 2
```plantuml
index : canvas
main o-- controllers.menu
main o-- controllers.stage
index o-- main

main : number canvasHeight
main : number canvasWidth
main : loop()


controllers.menu : window.addeventListener('keyDown')
controllers.menu : window.addeventListener('keyUp')
controllers.menu : keyListener()
controllers.menu : mouseListener()
controllers.menu -- model.menuModel
controllers.menu -- model.stageModel
controllers.menu -- model.recModel
controllers.stage -- model.stageModel
controllers.stage -- model.recModel



controllers.stage : window.addeventListener('keyDown')
controllers.stage : window.addeventListener('keyUp')
controllers.stage : keyListener()
controllers.stage : mouseListener()

model.stageModel -- model.recModel
model.stageModel -- model.platformModel
```