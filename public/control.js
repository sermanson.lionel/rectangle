/**
 * Copyright: frankarendpoth
 * https://github.com/frankarendpoth/frankarendpoth.github.io/tree/master/content/pop-vlog/javascript/2017/009-control
 */
let context, controller, recModel, loop;

context = document.querySelector("canvas").getContext("2d");

const canvasHeight = 360,
    canvasWidth = 640;
context.canvas.height = canvasHeight;
context.canvas.width = canvasWidth;

recModel = {
    height: 32,
    width: 32,
    jumping: true,
    x: 144,
    x_velocity: 0,
    y: 0,
    y_velocity: 0
};

controller = {
    left: false,
    right: false,
    up: false,
    keyListener: function (event) {
        let key_state = (event.type == "keydown") ? true : false;

        switch (event.keyCode) {
            case 37 || 113 :
                controller.left = key_state;
            break;
            case 38 || 111 :
                controller.up = key_state;
            break;
            case 39 || 114 :
                controller.right = key_state;
            break;
        }
    }
};

loop = function() {
    if (controller.up && recModel.jumping == false) {
        recModel.y_velocity -= 25
        recModel.jumping = true;
    }
    if (controller.left) {
        recModel.x_velocity -= 0.5;
    }
    if (controller.right) {
        recModel.x_velocity += 0.5;
    }

    recModel.y_velocity += 1.5; //gravity
    recModel.x += recModel.x_velocity;
    recModel.y += recModel.y_velocity;
    recModel.x_velocity *= 0.9; //friction
    // recModel.y_velocity *= 0.9; //friction

    // if rectangle is falling below floor line
    if (recModel.y > canvasHeight - 16 - 32) {
        recModel.jumping = false;
        recModel.y = canvasHeight - 16 - 32;
        recModel.y_velocity = 0;
    }

    // if rectangle is going off the left of the screen
    if (recModel.x < -32) {
        recModel.x = canvasWidth;
    } else if (recModel.x > canvasWidth) { // if rectangle goes past right
        recModel.x = -32;
    }

    context.fillStyle = "#202020";
    context.fillRect(0, 0, canvasWidth, canvasHeight); // x, y, width, height
    context.fillStyle = "#ff0000"; // hex for red
    context.beginPath();
    context.rect(recModel.x, recModel.y, recModel.width, recModel.height);
    context.fill();
    context.strokeStyle = "#202830";
    context.lineWidth = 4;
    context.beginPath();
    context.moveTo(0, canvasHeight - 16);
    context.lineTo(canvasWidth, canvasHeight-16);
    context.stroke();

    // call update when the browser is ready to draw again
    window.requestAnimationFrame(loop);
}


window.addEventListener("keydown", controller.keyListener);
window.addEventListener("keyup", controller.keyListener);
window.requestAnimationFrame(loop);

