const   express = require('express'),
        app = express(),
        port = 8010,
        bodyParser = require('body-parser'),
        fs = require('fs'),
        favicon = require('serve-favicon'),
        path = require('path')
    ;
    
    
    
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(bodyParser.json()) //required
app.use(express.static('public'))
//routes
app.get("/status", function (req, res) {
    res.statusCode = 200
    res.setHeader('Content-Type', 'application/json; charset=utf-8')
    res.end("{status: 'Ok! :)'}\n")
});

app.route('/')
    .get((req, res) => { 
        const index = fs.readFileSync(__dirname + '/public/index.html');
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html; charset=utf-8');
        res.end(index.toString())
    })
    ;

app.get('/platforms', (req, res)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    res.end(fs.readFileSync(__dirname + '/model/platforms.json'));
});

app.get('/rectangle', (req, res)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    res.end(fs.readFileSync(__dirname + '/model/recModel.json'));
});

app.listen(port, "127.0.0.1", () => {
    console.log(`Updater app listening on port ${port}.`);
});